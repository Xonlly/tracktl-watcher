[![pipeline status](https://gitlab.com/Xonlly/tracktl-watcher/badges/master/pipeline.svg)](https://gitlab.com/Xonlly/tracktl-watcher/commits/master)

# tracktl-watcher

This script allows to be notified of the state of the queue in a  http://track.tl  party.

# > 2.0

![example](example/example.gif)

The script use es6 promise, you may consider including a polyfill.

Build the source with `npm install && npm run build` and include the file [watcher.js](./dist/watcher.js), 

- https://gitlab.com/Xonlly/tracktl-watcher/raw/2.0.1/dist/watcher.js
- https://gitlab.com/Xonlly/tracktl-watcher/raw/2.0.2/dist/watcher.js
- https://gitlab.com/Xonlly/tracktl-watcher/raw/2.x.x/dist/watcher.js

Preview `npm start` and go to http://127.0.0.1:9070/

# Usage

You have a functional example in example/app.js.

```js
var Watcher = window.watcher;

var client = new Watcher();

// connect socket ( promise )
client.connect();

// Add error listener if is necessary 
client.onError(err => { ... })

// Get projectors
var projectors = client.getProjectors();
/*
projectors.player.getState():string ( playing, buffering, paused )
projectors.tracks.getFullQueue():array ( all tracks parsed and order by votes )
projectors.player.getPlaying():bool ( tracks is playing or not )
projectors.player.getPosition():number return the track playing position ( in second )
*/

// Add a state listener for data
client.addListener(state => { 
  /*
  state:
  state.party:object party joined 
  state.parties:array your parties
  state.user:object current user logged 
  state.users:array all users in party
  state.played:object played track infos
  state.tracks:array all tracks in party
  */

  /* default projectors examples: */
  var _state = {
    ...state,
    tracks: projectors.tracks.getFullQueue(), // parse and order tracks 
    selected: projectors.player.getSelected(), // get selected track
    playing: projectors.player.getPlaying(), // get playing track
  };
  
});

var pr;
pr = client.login('email', 'password');
// or
pr = client.loginByToken('tok');

// listen callback
pr.then(user => { ... }).catch(err => { ... }); // or listen ( onError method );
/* callback examples
user = {  
  "login":"exemple-watcher@watcher.fr",
  "mail":"exemple-watcher@watcher.fr",
  "features":{  

  },
  "logedFrom":"account",
  "infos":[  

  ],
  "id":95416,
  "name":"exemple-watcher",
  "pic":"upload/ano-b2_eezggd",
  "bigadmin":false,
  "token": "xxxxx",
}
*/

client.join(999 /* partyId */)
  .then(() => { return 'success' });
  .catch(err => { ... })

```

# < 2.0

# Requirements

The script use es6 promise, you may consider including a polyfill.

Build the source with `npm install && npm run build` and include the file [watcher.js](./dist/watcher.js), 

Preview `npm start` and go to http://127.0.0.1:9070/

# Usage

```javascript
var watcher = window.watcher

// Listen login errors:
watcher.user.on('error', function() {
  var error = watcher.user.getError();
});

// get user infos
var user = await watcher.login('exemple-watcher@watcher.fr', 'exemple-watcher');

// or
watcher.authByToken('tokenInUser');

// or
watcher.login('exemple-watcher@watcher.fr', 'exemple-watcher')
  .then(function () {
    var user = watcher.user.getInfos();
    // user:
    /*
    {  
      "login":"exemple-watcher@watcher.fr",
      "mail":"exemple-watcher@watcher.fr",
      "features":{  

      },
      "logedFrom":"account",
      "infos":[  

      ],
      "id":95416,
      "name":"exemple-watcher",
      "pic":"upload/ano-b2_eezggd",
      "bigadmin":false,
      "token": "xxxxx",
    }
    */
    
    var parties = watcher.user.getParties();
    // parties:
    /*
    [  
      {  
        "id":101233,
        "name":"exemple-watcher",
        "pic":"upload/ano-b2_eezggd",
        "slug":"exemplewatcher",
        "pic_back":null,
        "owner_id":95416,
        "description":null,
        "location":"",
        "max_song_by_user":4,
        "setting":{  

        },
        "private":false,
        "access":{  

        },
        "password":false,
        "loop":false
      }
    ]
    */
    
    watcher.user.on( 'change', function() {
      
    })
} )
  .catch(e => {
    // login error:
    // e.error
    // {type: "not found", detail: "wrong combinaison mail / password"}
  })

watcher.connect( partyId )
    .then(function () {

        // connected to the party

        // get the selected track ( the one which is either paused or palying )
        var selected = watcher.trackStore.getSelected()

        selected.title
        selected.artist
        selected.vote
        ..

        // get the ordered queue of next tracks
        var queue = watcher.trackStore.getFullQueue()

        // be notified when the queue change
        watcher.trackStore.on( 'change', function() {
            // the queue as changed ( reordered, track added, track removed, track altered ... )
        })



        // get playing information

        // return true if the player is playing
        watcher.mainPlayer.getPlaying()

        // be notified when the player change
        watcher.mainPlayer.on( 'change', function() {

        })
    })


```

see also the [demo](./example/app.js)

# Spec

```javascript
watcher.trackStore.getSelected()
// return a track :
// {
//      id,
//
//      title,
//      artist,
//      duration,           // ( in second )
//      coverArt,
//      coverArt_big,
//
//      vote,
//
//      adder : {
//          id,
//          name,
//          pic,
//      }
// }

watcher.trackStore.getFullQueue()
// return a array of tracks

watcher.mainPlayer.getPlaying()
// return a boolean

watcher.mainPlayer.getPosition()
// return the track playing position ( in second )

```
