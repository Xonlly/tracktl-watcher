CHANGELOG
===
📦 2.0.2
- 🔧 add tests by ci
- 🔧 refactor example
- 🔧 refactor watcher
- 🐛 fix join errors
- 🐛 fix sockets errors
- 🐛 fix login errors

📦 1.1.7
- 🐛 fix: add auth token to user.
- 🔧 auth: add auth by token

📦 1.1.6
- 🔧 refactor: all auth system
