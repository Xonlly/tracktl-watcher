export const endpoint = process.env.REVIEW
  ? 'wss://live.tracktl.review'
  : 'wss://live.tracktl.com';

export const port = 443;

export const transports = process.env.CI
  ? ['websocket']
  : ['polling', 'websocket'];

export const upgrade = true;

export const protocol = 'wss:';
