import Watcher from '../watcher';
import expect from 'expect';
import * as config from '../config';

const partyId = process.env.REVIEW ? 1 : 74539;
const userKeys = [
  'login',
  'mail',
  'features',
  'logedFrom',
  'infos',
  'id',
  'name',
  'pic',
  'bigadmin',
  'token',
];

describe('watcher', function() {
  before(async () => {
    this.client = new Watcher();

    await this.client.connect();

    // need fix that ...
    // await new Promise(resolve => setTimeout(() => resolve(), 500));
  });

  it('login', async () => {
    const id = process.env.REVIEW
      ? { user: 'test', pass: 'test' }
      : { user: 'exemple-watcher@watcher.fr', pass: 'exemple-watcher' };

    const user = await this.client.login(id.user, id.pass);

    expect(user).toIncludeKeys(userKeys);

    this.token = user.token;
  });

  it('should login by token', async () => {
    const user = await this.client.loginByToken(this.token);

    expect(user).toIncludeKeys(userKeys);

    // expect(parties.length >= 1).toBe(true);
  });

  it('should be join a party', async () => {
    await this.client.join(partyId);
    expect(this.client.state.party.id).toBe(partyId);
  });

  it('should get the selected track', () => {
    const projectors = this.client.getProjectors();
    const track = projectors.player.getSelected();

    expect(track.title).toBeTruthy();
    expect(track.artist).toBeTruthy();
    expect(track.vote).toBeTruthy();
    expect(track.coverArt).toBeTruthy();
    expect(track.coverArt_big).toBeTruthy();
    expect(track.duration).toBeTruthy();
    expect(track.adder.name).toBeTruthy();
    expect(track.adder.pic).toBeTruthy();
  });

  it('should get the queued tracks', () => {
    const projectors = this.client.getProjectors();
    const tracks = projectors.tracks.getFullQueue();

    expect(tracks.length).toBeGreaterThan(0);

    tracks.slice(0, 3).forEach(track => {
      expect(track.title).toExist('title');
      expect(track.artist).toExist('artist');
      expect(track.coverArt).toExist('coverArt');
      expect(track.coverArt_big).toExist('coverArt_big');
      expect(track.duration).toExist('duration');
      expect(track.adder.name).toExist('adder');
      expect(track.adder.pic).toExist('adder');
    });
  });

  it('should get the selected track playing state', () => {
    const projectors = this.client.getProjectors();
    const playing = projectors.player.getPlaying();

    expect(playing).toNotBe(null);
  });

  it('should get the selected track playing state string', () => {
    const projectors = this.client.getProjectors();
    const playing = projectors.player.getPlaying();

    expect(playing).toNotBe(null);
  });

  it('should get the selected track position', () => {
    const projectors = this.client.getProjectors();
    const position = projectors.player.getPosition();

    expect(typeof position).toBe('number');
  });
});
