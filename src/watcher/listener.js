export default class Listen {
  socket = false;

  state = {
    user: false,
    users: [],
    tracks: [],
    party: false,
    parties: {
      mine: [],
    },
  };

  updated = () => this.callback(this.state);

  listenUser = () => {
    if (!this.socket) return false;

    this.socket.on('auth', data => {
      const state = this.state;
      Object.assign(state, { user: data.user });
      this.updated();
    });

    this.socket.on('party:init', data => {
      const state = this.state;
      Object.assign(state, { party: data });
      this.updated();
    });

    this.socket.on('party:get-mine', data => {
      const state = this.state;
      Object.assign(state, { parties: data.parties });
      this.updated();
    });
  };

  listenParty = () => {
    if (!this.socket) return false;

    this.socket.on('trackList:init', data => {
      const state = this.state;
      Object.assign(state, data);
      this.updated();
    });
    this.socket.on('userList:patch', data => {
      const state = this.state;
      const users = state.users.map(user => {
        const u = data.users && data.users.find(({ id }) => user.id == id);
        return { ...user, ...u };
      });
      state.users = users;
      this.updated();
    });

    this.socket.on('trackList:patch', data => {
      const state = this.state;
      const played = data.played
        ? { ...state.played, ...data.played }
        : state.played;

      const tracks = [
        ...state.tracks
          .filter(
            ({ id }) => !data.removeIds || !data.removeIds.some(u => u == id),
          )
          .map(track => {
            const t =
              data.tracks && data.tracks.find(({ id }) => track.id == id);

            return {
              ...track,
              ...t,
            };
          }),
        ...(data.tracks || [])
          .filter(({ id }) => !state.tracks.some(u => u.id == id)),
      ];
      state.tracks = tracks;
      state.played = played;
      this.updated();
    });
  };

  addListener = callback => {
    if (!this.socket || !callback) return false;
    this.callback = callback;
  };
}
