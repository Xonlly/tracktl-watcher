import crypto from 'crypto';
import { create as createSocket } from './socket';
import * as config from '../config';
import * as projector from '../projector';
import Listener from './listener';

export default class Watcher extends Listener {
  constructor() {
    super();

    this.socket = createSocket({
      protocol: config.protocol,
      endpoint: config.endpoint,
      port: config.port,
      transports: config.transports,
      upgrade: config.upgrade,
    });

    this.callback = () => {};
    this.onError = () => {};

    this.socket.on('request-error', data => {
      this.onError(data);
    });

    this.listenUser();
    this.listenParty();
  }

  parseLogin = login => ({
    ...login.user,
    token: login.token,
  });

  getProjectors = () => {
    const methods = {
      player: {
        getPlaying: projector.getPlaying.bind(null, this.state),
        getPercent: projector.getPercent.bind(null, this.state),
        getSelected: projector.getSelected.bind(null, this.state),
        getState: projector.getState.bind(null, this.state),
        getPosition: projector.getPosition.bind(null, this.state),
      },
      user: {
        getInfos: projector.getUserInfos.bind(null, this.state),
        getParties: projector.getUserParties.bind(null, this.state),
      },
      tracks: {
        getFullQueue: projector.getFullQueue.bind(null, this.state),
      },
      getError: projector.getError.bind(null, this.state),
    };
    return methods;
  };

  connect = () => this.socket.connect();

  // login user
  login = async (mail, password) => {
    if (!this.socket)
      throw { type: 'unauthorized', detail: 'init a socket client' };

    const md5password = crypto
      .createHash('md5')
      .update(password)
      .digest('hex');

    const login = await this.socket.emit('auth:password', {
      mail,
      md5password,
    });

    await this.socket.emit('party:get-mine');
    const user = this.parseLogin(login);
    this.state.user = user;
    return user;
  };

  // login user by token
  loginByToken = async token => {
    if (!this.socket)
      throw { type: 'unauthorized', detail: 'init a socket client' };

    const login = await this.socket.emit('auth:token', { token });
    await this.socket.emit('party:get-mine');
    const user = this.parseLogin(login);
    this.state.user = user;
    return user;
  };

  // join a party
  join = async partyId => {
    if (!this.state.user)
      throw { type: 'unauthorized', detail: 'please login a user' };

    await this.socket.emit('party:join', { partyId });
    await this.socket.emit('party:get', { partyId });
    await this.socket.emit('trackList:get');
  };

  // search a party
  partySearch = async (pattern, limit = 10) => {
    return this.state.parties.mine;
  };
}
