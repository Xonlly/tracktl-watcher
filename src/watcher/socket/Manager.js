// https://github.com/socketio/socket.io-client/blob/master/lib/manager.js
import Emitter from 'events';
import createDebug from 'debug';
import eio from 'engine.io-client';
import Backoff from 'backo2';

const debug = createDebug('system:live:manager');

/**
 * Helper for subscriptions.
 *
 * @param {Object|EventEmitter} obj with `Emitter` mixin or `EventEmitter`
 * @param {String} event name
 * @param {Function} callback
 * @api public
 */

function on(obj, ev, fn) {
  obj.on(ev, fn);
  return {
    destroy() {
      obj.removeListener(ev, fn);
    },
  };
}

export default class Manager extends Emitter {
  readyState = 'closed';
  subs = [];

  /**
   * `Manager` constructor.
   *
   * @param {String} engine instance or engine uri/opts
   * @param {Object} options
   * @api public
   */
  constructor(uri, opts = {}) {
    super();
    this.opts = opts;
    this.backoff = new Backoff({
      min: opts.reconnectionDelay || 1000,
      max: opts.reconnectionDelayMax || 5000,
      jitter: opts.randomizationFactor || 0.5,
    });
    this.uri = uri;
    this.lastPing = null;
    if (opts.autoConnect !== false) this.connect();
  }

  maybeReconnectOnOpen() {
    // Only try to reconnect if it's the first time we're connecting
    if (!this.reconnecting && this.backoff.attempts === 0) {
      // keeps reconnection from firing twice for the same reconnection loop
      this.reconnect();
    }
  }

  connect(_internalCallback) {
    if (this.readyState.startsWith('open')) return;

    debug('opening %s', this.uri);
    this.engine = eio(this.uri, this.opts);
    const socket = this.engine;

    this.readyState = 'opening';
    this.skipReconnect = false;

    const openSub = on(socket, 'open', () => {
      this.onopen();
      if (_internalCallback) _internalCallback();
    });

    const errorSub = on(socket, 'error', data => {
      this.cleanup();
      this.readyState = 'closed';
      this.emit('connect_error', data);
      if (_internalCallback) {
        const err = new Error('Connection error');
        err.data = data;
        _internalCallback(err);
      } else {
        this.maybeReconnectOnOpen();
      }
    });

    this.subs.push(openSub);
    this.subs.push(errorSub);
  }

  /**
   * Clean up transport subscriptions.
   *
   * @api private
   */
  cleanup() {
    debug('cleanup');

    const subsLength = this.subs.length;
    for (let i = 0; i < subsLength; i++) {
      const sub = this.subs.shift();
      sub.destroy();
    }

    this.lastPing = null;
  }

  /**
   * Called upon transport open.
   *
   * @api private
   */
  onopen = () => {
    // clear old subs
    this.cleanup();

    // mark as open
    this.readyState = 'open';
    this.emit('open');

    // add new subs
    const socket = this.engine;
    this.subs.push(on(socket, 'message', this.onmessage));
    this.subs.push(on(socket, 'ping', this.onping));
    this.subs.push(on(socket, 'pong', this.onpong));
    this.subs.push(on(socket, 'error', this.onerror));
    this.subs.push(on(socket, 'close', this.onclose));
  };

  /**
   * Called upon engine close.
   *
   * @api private
   */
  onclose = reason => {
    debug('onclose');

    this.cleanup();
    this.backoff.reset();
    this.readyState = 'closed';
    this.emit('close', reason);

    if (!this.skipReconnect) {
      this.reconnect();
    }
  };

  /**
   * Called upon successful reconnect.
   *
   * @api private
   */
  onreconnect = () => {
    const attempt = this.backoff.attempts;
    this.reconnecting = false;
    this.backoff.reset();
    this.emit('reconnect', attempt);
  };

  onmessage = message => {
    this.emit('message', message);
  };

  /**
   * Called upon a ping.
   *
   * @api private
   */
  onping = () => {
    this.lastPing = new Date();
    this.emit('ping');
  };

  /**
   * Called upon a packet.
   *
   * @api private
   */
  onpong = () => {
    this.emit('pong', new Date() - this.lastPing);
  };

  /**
  * Called upon socket error.
  *
  * @api private
  */
  onerror = err => this.emit('error', err);

  destroy() {
    this.close();
  }

  /**
   * Close the current socket.
   *
   * @api private
   */
  close() {
    debug('disconnect');

    this.skipReconnect = true;
    this.reconnecting = false;
    if (this.readyState === 'opening') {
      // `onclose` will not fire because
      // an open event never happened
      this.cleanup();
    }
    this.backoff.reset();
    this.readyState = 'closed';
    if (this.engine) this.engine.close();
  }

  /**
   * Attempt a reconnection.
   *
   * @api private
   */
  reconnect() {
    if (this.reconnecting || this.skipReconnect) return this;

    const delay = this.backoff.duration();
    debug('will wait %dms before reconnect attempt', delay);

    this.reconnecting = true;
    const timer = setTimeout(() => {
      if (this.skipReconnect) return;

      debug('attempting reconnect');
      this.emit('reconnect_attempt', this.backoff.attempts);
      this.emit('reconnecting', this.backoff.attempts);

      // check again for the case socket closed in above events
      if (this.skipReconnect) return;

      this.connect(err => {
        if (err) {
          debug('reconnect attempt error');
          this.reconnecting = false;
          this.reconnect();
          this.emit('reconnect_error', err.data);
        } else {
          debug('reconnect success');
          this.onreconnect();
        }
      });
    }, delay);

    this.subs.push({
      destroy() {
        clearTimeout(timer);
      },
    });
  }

  send(data, options) {
    return this.engine.send(data, options);
  }
}
