import EventEmitter from 'events';
import Manager from './Manager';

const genUID = () =>
  Math.random()
    .toString(34)
    .slice(2);

const inIframe = (window = global.window) => {
  if (typeof window === 'undefined') return false;

  try {
    if (window.frameElement) return true;
  } catch (e) {
    return false;
  }

  try {
    return window.self !== window.top;
  } catch (e) {
    return true;
  }
};

/**
 *
 * handle
 *   - reconnection
 *   - event subscription ( with promise mecanism )
 *
 */

export const create = (options = {}) => {
  const manager = new Manager(options.endpoint, {
    autoConnect: false,
    upgrade: options.upgrade,
    transports: options.transports,
    reconnectionDelay: options.reconnectionDelay,
    reconnectionDelayMax: options.reconnectionDelayMax,
  });
  const ee = new EventEmitter();

  const requests = new Map();

  const clean = () => {
    if (process.env.NODE_ENV !== 'production') {
      requests.forEach(({ timeout, reject }) => {
        clearTimeout(timeout);
        return reject(new Error('timeout'));
      });
    }

    requests.clear();
  };

  manager
    .on('error', err => ee.emit('error', err))
    .on('open', () => ee.emit('open'))
    .on('close', () => {
      clean();
      // notify
      ee.emit('disconnect');
    })
    .on('message', m => {
      const x = JSON.parse(m);
      const payload = x.payload || {};
      const meta = x.meta;

      // is the event a response
      if (meta && requests.has(meta.key)) {
        const { reject, resolve, timeout } = requests.get(meta.key);
        if (process.env.NODE_ENV !== 'production') {
          clearTimeout(timeout);
        }

        requests.delete(meta.key);

        if (payload.error) reject(payload);
        else resolve(payload);

        // xxx arthur : should we still emit the event ?
        // return
      }

      // notify
      ee.emit(x.type, payload);
    });

  const api = {
    connect: () => {
      return new Promise(resolve => {
        manager.once('open', resolve);
        manager.connect();
      });
    },

    /**
     * unsubcribe to an event
     *
     * @return {function}   function to call to unsubcribe
     */
    on: (type, handler) => {
      ee.on(type, handler);
      return api;
    },

    /**
     * unsubcribe to the event
     */
    off: (type, handler) => {
      ee.off(type, handler);
      return api;
    },

    /**
     * emit an event
     * act as a promise
     * /!\ for the promise to resolve, the server must implement a response with meta key for this event
     */
    emit: (type, payload = {}) =>
      new Promise((resolve, reject) => {
        if (manager.readyState !== 'open') {
          console.warn(`cannot send "${type}", socket null`);
          return;
        }

        const key = genUID();
        requests.set(key, {
          resolve,
          reject,
          timeout:
            process.env.NODE_ENV !== 'production' &&
            setTimeout(() => console.warn('timeout', { type, payload }), 5000),
        });
        manager.send(JSON.stringify({ type, payload, meta: { key } }));
      }),

    /**
     * return the socketId
     */
    id: () => manager.engine && manager.engine.id,

    /**
     * clean up
     */
    destroy: () => {
      ee.removeAllListeners();
      manager.destroy();
      clean();
    },

    _close: () => {
      clean();
      manager.engine.close();
    },
  };

  // dev methodes
  if (typeof window !== 'undefined' && process.env.NODE_ENV !== 'production') {
    window.forceDisconnect = () => {
      manager.close();
    };

    window.connect = () => {
      manager.connect();
    };
  }

  return api;
};
