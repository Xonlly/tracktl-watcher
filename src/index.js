import * as config from "./config";
import Watcher from './watcher';

const watcher = Watcher;

export default watcher;
if ("undefined" != typeof window) window.watcher = watcher;
