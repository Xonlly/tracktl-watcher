const buildUserPicUrl = uri =>
  `https://res.cloudinary.com/jukeo-net/image/${uri}`;

const formatTrack = (users, x = {}) => {
  const adder = users.find(({ id }) => id === x.adderId) || {};
  return {
    artist: x.song && x.song.artist,
    title: x.song && x.song.name,
    vote: x.votes ? x.votes.length : 0,
    coverArt: x.song && x.song.coverArt,
    coverArt_big: x.song && x.song.coverArt_big,
    duration: x.song && x.song.duration,
    id: x.id,
    adder: {
      ...adder,
      pic: buildUserPicUrl(adder.pic),
    },
  };
};

export const getSelected = ({ tracks, users, played }) =>
  played &&
  played.id &&
  formatTrack(users, tracks.find(({ id }) => id === played.id));

export const getFullQueue = ({ tracks, users, played }) =>
  tracks
    .filter(({ id }) => id != (played && played.id))
    .sort((a, b) => {
      if (!!a.priority != !!b.priority) return a.priority ? -1 : 1;

      if (a.voteCount != b.voteCount)
        return a.voteCount < b.voteCount ? 1 : -1;

      return +a.id > +b.id ? 1 : -1;
    })
    .map(formatTrack.bind(null, users));

export const getPlaying = ({ played }) => !!(played && played.playing);

export const getState = ({ played }) => played && played.state;

export const getPosition = ({ played, tracks }) => {
  if (!played || !played.id) return 0;
  return played.state === 'playing'
    ? (Date.now() - played.position) / 1000
    : played.position / 1000;
};

export const getPercent = state => {
  const selected = getSelected(state);
  const position = getPosition(state);

  if (!selected || !selected.duration) return 0;

  return position * 100 / selected.duration;
};

export const getUserInfos = ({ user }) => user || false;

export const getUserParties = ({ parties }) => parties || [];

export const getError = ({ error }) => error || false;
