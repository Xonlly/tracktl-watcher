const path = require("path");
const webpack = require("webpack");

const production = "production" == process.env.NODE_ENV;

module.exports = {
  entry: {
    "babel-polyfill": 'babel-polyfill',
    "watcher-example": path.join(__dirname, "example/index.js"),
    watcher: path.join(__dirname, "src/index.js")
  },

  output: {
    path: path.join(__dirname, "dist"),
    filename: "[name].js"
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          {
            loader: "babel-loader"
          }
        ]
      }
    ]
  },

  plugins: production
    ? [
        // minify
        new webpack.optimize.UglifyJsPlugin({ compress: { warnings: false } })
      ]
    : [],
  devtool: "source-map"
};
