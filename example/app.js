import React, { Component } from 'react';

export class AppState extends Component {
  state = {
    statusJoin: 'not joined',
    statusLogin: 'not connected',
    selected: null,
    state: false,
    position: 0,
    percent: 0,
  };

  join = partyId => {
    this.setState({ statusJoin: 'connecting' });

    this.watcher
      .join(partyId)
      .then(() => this.setState({ statusJoin: 'connected' }))
      .catch(
        error => console.log(error) || this.setState({ statusJoin: 'failed' }),
      );
  };

  login = (email, password) => {
    this.setState({ statusLogin: 'connecting' });

    this.watcher
      .login(email, password)
      .then(() => this.setState({ statusLogin: 'logged' }))
      .catch(err => this.setState({ statusLogin: 'error...' }));
  };

  componentDidMount() {
    const { watcher } = this.props;

    this.watcher = new watcher();

    this.watcher.connect();

    this.parsers = this.watcher.getProjectors();

    this.intervalPosition = setInterval(() => {
      this.setState({
        position: this.parsers.player.getPosition(),
        percent: this.parsers.player.getPercent(),
      });
    }, 100);

    this.watcher.addListener((state, methods) => {
      const _state = {
        ...state,
        tracks: this.parsers.tracks.getFullQueue(),
        selected: this.parsers.player.getSelected(),
        playing: this.parsers.player.getPlaying(),
      };

      this.setState({ state: _state });
    });
  }

  componentWillUnmout() {
    clearInterval(this.intervalPosition);
  }

  render() {
    return <App {...this.state} join={this.join} login={this.login} />;
  }
}

class App extends Component {
  state = {
    login: '',
    password: '',
    partyId: '',
  };

  setLogin = login => this.setState({ login });

  setPassword = password => this.setState({ password });

  setPartyId = partyId => this.setState({ partyId });

  login = () => this.props.login(this.state.login, this.state.password);

  join = () => this.props.join(this.state.partyId);

  joinById = id => this.props.join(id);

  render() {
    const { statusLogin, statusJoin, state, position, percent } = this.props;
    const { login, password, partyId } = this.state;

    return (
      <div>
        <span>
          {statusLogin} | {statusJoin}
        </span>
        {statusLogin == 'not connected' && (
          <div>
            <input
              type="email"
              value={login}
              placeholder="login"
              onChange={e => this.setLogin(e.target.value)}
            />
            <input
              type="password"
              placeholder="password"
              value={password}
              onChange={e => this.setPassword(e.target.value)}
            />
            <input type="button" value="login" onClick={this.login} />
          </div>
        )}
        {statusLogin == 'logged' &&
        state.parties && (
          <div>
            <h4>My parties</h4>
            {state.parties.map(x => (
              <span
                key={x.id}
                style={{ cursor: 'pointer' }}
                onClick={() => this.joinById(x.id)}
              >
                {x.id + ' '}
              </span>
            ))}
          </div>
        )}
        {statusLogin == 'logged' && (
          <div>
            <input
              type="text"
              value={partyId}
              placeholder="party id (ex 74539 )"
              onChange={e => this.setPartyId(e.target.value)}
            />
            <button onClick={this.join}>go</button>
          </div>
        )}
        {statusJoin == 'connected' && (
          <Player
            played={state.played}
            position={position}
            percent={percent}
            selected={state.selected}
          />
        )}
        {statusJoin == 'connected' && <List tracks={state.tracks} />}
      </div>
    );
  }
}

const List = ({ tracks }) => (
  <ul>{tracks.map(track => <Track key={track.id} {...track} />)}</ul>
);

const Track = ({ adder, title, vote, coverArt }) => (
  <li>
    <span>
      <span>
        <img src={coverArt} width={20} height={20} />
      </span>
      <span>{title}</span>
      <span>[ vote: {vote} ]</span>
    </span>

    {adder && (
      <span>
        <span> -- added by {adder.name}</span>
        <img src={adder.pic} width={20} height={20} />
      </span>
    )}
  </li>
);

const Player = ({ selected, playing, position, percent }) => (
  <h1>
    <span
      dangerouslySetInnerHTML={{
        __html: playing ? '&#9654;' : '&#10074;&#10074;',
      }}
    />
    <span>
      position: {position.toFixed(2)} | percent: {percent.toFixed(2)}%
    </span>
    <span>
      <Track {...selected} />
    </span>
  </h1>
);
